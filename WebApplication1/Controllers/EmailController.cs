﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MailKit.Net.Smtp;
using MimeKit;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        [HttpPost]
        [Route("DailyNotes")]
        public async Task<IActionResult> SendEmail([FromBody] EmailRequest emailRequest)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid request body.");
                }

                var emailMessage = new MimeMessage();
                emailMessage.From.Add(new MailboxAddress("Jessyca Fritsch", "jessyca.fritsch15@ethereal.email"));
                emailMessage.To.Add(new MailboxAddress("", emailRequest.EmailTo));
                emailMessage.Subject = "Daily Notes";

                var bodyBuilder = new BodyBuilder();

                bodyBuilder.HtmlBody = "<div style='border: 1px solid black; padding: 10px;'><h2>Daily Notes</h2><ul>";
                foreach (var note in emailRequest.Notes)
                {
                    bodyBuilder.HtmlBody += $"<li>{note}</li>";
                }
                bodyBuilder.HtmlBody += "</ul></div>";

                emailMessage.Body = bodyBuilder.ToMessageBody();

                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync("smtp.ethereal.email", 587, false);
                    await client.AuthenticateAsync("jessyca.fritsch15@ethereal.email", "XHg2ZckheQ4TNFrwtP");
                    await client.SendAsync(emailMessage);
                    await client.DisconnectAsync(true);
                }

                return Ok("Email sent successfully!");
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        public class EmailRequest
        {
            public string EmailTo { get; set; }
            public List<string> Notes { get; set; }
        }
    }
}
