﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MySql.Data.MySqlClient;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        #region Configuration
        private readonly IConfiguration _configuration;
        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        #endregion

        #region Route
        [HttpPost]
        [Route("user")]
        public IActionResult Login([FromBody] LoginModel userLogin)
        {
            var user = ValidateUser(userLogin);
            if (user != null)
            {
                var token = GenerateToken(user);
                return Ok(token);
            }
            return NotFound("User Not Found");
        }
        #endregion

        #region Private Function
        private DataModel ValidateUser(LoginModel userLogin)
        {
            List<DataModel> users = new List<DataModel>();

            using (MySqlConnection conn = new MySqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string query = "select * from login";
                MySqlCommand cmd = new MySqlCommand(query, conn);
                MySqlDataAdapter dtAdapter = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dtAdapter.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    DataModel user = new DataModel();
                    user.UserId = Convert.ToInt32(dr["id"].ToString());
                    user.Name = dr["Name"].ToString();
                    user.UserName = dr["Username"] == (object)DBNull.Value ? "" : dr["Username"].ToString();
                    user.Email = dr["Email"] == (object)DBNull.Value ? "" : dr["Email"].ToString();
                    user.Password = dr["Password"] == (object)DBNull.Value ? "" : dr["Password"].ToString();
                    users.Add(user);
                }

                conn.Close();
            }
            var currentUser = users.FirstOrDefault(u => u.UserName.ToLower() == userLogin.UserName.ToLower() && u.Password == userLogin.Password);

            if (currentUser != null)
            {
                return currentUser;
            }
            return null;
        }
        private string GenerateToken(DataModel user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier, user.UserName),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var token = new JwtSecurityToken(
                _configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        #endregion


    }
}
